<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CCAASeeder extends Seeder
{
    public function run()
    {
        DB::table('ccaa')->insert([
            'nombre' => 'Cataluña',
            'pais_id' => 1,
        ]);
    }
}
