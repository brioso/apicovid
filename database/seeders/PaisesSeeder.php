<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PaisesSeeder extends Seeder
{
    public function run()
    {
        DB::table('paises')->insert([
            'nombre' => 'España',
        ]);
    }
}
