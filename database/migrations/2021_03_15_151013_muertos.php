<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Muertos extends Migration
{
    public function up()
    {
        Schema::create('muertos', function (Blueprint $table) {
            $table->id();
            $table->date('fecha');
            $table->integer('ccaas_id');
            $table->integer('numero');
            $table->foreign('ccaas_id')->references('id')->on('ccaa')->onDelete('cascade');

        });
    }

    public function down()
    {
        Schema::dropIfExists('muertos');
    }
}
