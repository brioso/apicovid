<?php

namespace App\Http\Controllers;

use App\Http\Resources\CovidCollection;
use App\Models\muertos;
use Illuminate\Http\Request;
use App\Http\Resources\ShowResource;
use Illuminate\Support\Facades\DB;

class muertosController extends Controller
{
    public function index()
    {
        //
    }

    public function showAll()
    {
        $muertos = muertos::all();
        if (! $muertos)
        {
            return response()->json(['errors'=>Array(['code'=>404,'message'=>'No se encuentra un fabricante con ese código.'])],404);
        }

        return response()->json(['status'=>'ok','data'=>$muertos],200);
    }

    public function store(Request $request)
    {
        $muertos = new Ia14();
        $muertos->fecha = $request->fecha;
        $muertos->ccaas_id = $request->ccaas_id;
        $muertos->numero = $request->numero;
        $muertos->save();
        return response()->json($muertos);
    }

    public function show($id)
    {

        $muertos = muertos::where("fecha",$id)->first();
        if (! $muertos)
        {
            return response()->json(['errors'=>Array(['code'=>404,'message'=>'No se encuentra un fabricante con ese código.'])],404);
        }
        return new ShowResource($muertos);
    }


    public function update(Request $request)
    {
        $muertos = muertos::where("fecha",$request->fecha)->first();
        $muertos->fecha = $request->fecha;
        $muertos->ccaas_id = $request->ccaas_id;
        $muertos->numero = $request->numero;
        $muertos->save();
        return response()->json($muertos);
    }


    public function destroy($id)
    {
        $muertos = muertos::where("fecha",$id)->first();

        if ($muertos)
        {
            $muertos->delete();
        } else
        {
            return response()->json(['errors'=> Array(['code'=>404,'message'=>'no'])]);
        }

        return response()->json(null);
    }



    public function showPart($id1,$id2)
    {

        if ($id1 > $id2 )
            return response()->json(['errors'=>Array(['code'=>404,'message'=>'La fecha inicial es mayor'])],404);

        $muertos = DB::select(DB::raw("select * from muertos where fecha BETWEEN '$id1' and '$id2' "));

        if (! $muertos)
        {
            return response()->json(['errors'=>Array(['code'=>404,'message'=>'No se encuentra un fabricante con ese código.'])],404);
        }

        return new CovidCollection($muertos);

    }

}
