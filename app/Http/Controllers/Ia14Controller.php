<?php

namespace App\Http\Controllers;

use App\Http\Resources\CovidCollection;
use App\Models\ia14;
use Illuminate\Http\Request;
use App\Http\Resources\ShowResource;
use Illuminate\Support\Facades\DB;

class Ia14Controller extends Controller
{
    public function index()
    {
        //
    }

    public function showAll()
    {
        $ia14 = ia14::all();
        if (! $ia14)
        {
            return response()->json(['errors'=>Array(['code'=>404,'message'=>'No se encuentra un fabricante con ese código.'])],404);
        }

        return response()->json(['status'=>'ok','data'=>$ia14],200);
    }

    public function store(Request $request)
    {
        $ia14 = new Ia14();
        $ia14->fecha = $request->fecha;
        $ia14->ccaas_id = $request->ccaas_id;
        $ia14->incidencia = $request->incidencia;
        $ia14->save();
        return response()->json($ia14);
    }

    public function show($id)
    {

        $ia14 = ia14::where("fecha",$id)->first();
        if (! $ia14)
        {
            return response()->json(['errors'=>Array(['code'=>404,'message'=>'No se encuentra un fabricante con ese código.'])],404);
        }
        return new ShowResource($ia14);
    }


    public function update(Request $request)
    {
        $ia14 = ia14::where("fecha",$request->fecha)->first();
        $ia14->fecha = $request->fecha;
        $ia14->ccaas_id = $request->ccaas_id;
        $ia14->incidencia = $request->incidencia;
        $ia14->save();
        return response()->json($ia14);
    }


    public function destroy($id)
    {
        $ia14 = ia14::where("fecha",$id)->first();

        if ($ia14)
        {
            $ia14->delete();
        } else
        {
            return response()->json(['errors'=> Array(['code'=>404,'message'=>'no'])]);
        }

        return response()->json(null);
    }



    public function showPart($id1,$id2)
    {

        if ($id1 > $id2 )
            return response()->json(['errors'=>Array(['code'=>404,'message'=>'La fecha inicial es mayor'])],404);

        $ia14 = DB::select(DB::raw("select * from ia14 where fecha BETWEEN '$id1' and '$id2' "));

        if (! $ia14)
        {
            return response()->json(['errors'=>Array(['code'=>404,'message'=>'No se encuentra un fabricante con ese código.'])],404);
        }

        return new CovidCollection($ia14);

    }

}
