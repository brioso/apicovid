<?php

namespace App\Http\Controllers;

use App\Http\Resources\CovidCollection;
use App\Models\casos;
use Illuminate\Http\Request;
use App\Http\Resources\ShowResource;
use Illuminate\Support\Facades\DB;

class casosController extends Controller
{
    public function index()
    {
        //
    }

    public function showAll()
    {
        $casos = casos::all();
        if (! $casos)
        {
            return response()->json(['errors'=>Array(['code'=>404,'message'=>'No se encuentra un fabricante con ese código.'])],404);
        }

        return response()->json(['status'=>'ok','data'=>$casos],200);
    }

    public function store(Request $request)
    {
        $casos = new Ia14();
        $casos->fecha = $request->fecha;
        $casos->ccaas_id = $request->ccaas_id;
        $casos->numero = $request->numero;
        $casos->save();
        return response()->json($casos);
    }

    public function show($id)
    {

        $casos = casos::where("fecha",$id)->first();
        if (! $casos)
        {
            return response()->json(['errors'=>Array(['code'=>404,'message'=>'No se encuentra un fabricante con ese código.'])],404);
        }
        return new ShowResource($casos);
    }


    public function update(Request $request)
    {
        $casos = casos::where("fecha",$request->fecha)->first();
        $casos->fecha = $request->fecha;
        $casos->ccaas_id = $request->ccaas_id;
        $casos->numero = $request->numero;
        $casos->save();
        return response()->json($casos);
    }


    public function destroy($id)
    {
        $casos = casos::where("fecha",$id)->first();

        if ($casos)
        {
            $casos->delete();
        } else
        {
            return response()->json(['errors'=> Array(['code'=>404,'message'=>'no'])]);
        }

        return response()->json(null);
    }



    public function showPart($id1,$id2)
    {

        if ($id1 > $id2 )
            return response()->json(['errors'=>Array(['code'=>404,'message'=>'La fecha inicial es mayor'])],404);

        $casos = DB::select(DB::raw("select * from casos where fecha BETWEEN '$id1' and '$id2' "));

        if (! $casos)
        {
            return response()->json(['errors'=>Array(['code'=>404,'message'=>'No se encuentra un fabricante con ese código.'])],404);
        }

        return new CovidCollection($casos);

    }

}
